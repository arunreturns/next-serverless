import Link from './ActiveLink';
import { Navbar, Nav } from 'react-bootstrap';

const HeaderLinks = [
  { linkName: "Home", linkUrl: "/" },
  { linkName: "Create API", linkUrl: "/create-api" },
  { linkName: "Schema Creator", linkUrl: "/schema-creator" },
]
const Header = () => (
  <Navbar bg="light" expand="lg">
    <Navbar.Brand href="/"><span className="app-header fascinate-inline">API Creator</span></Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
        {HeaderLinks.map(headerLink => 
          <Link activeClassName="text-dark" href={headerLink.linkUrl} key={headerLink.linkName}><Nav.Link href={headerLink.linkUrl}>{headerLink.linkName}</Nav.Link></Link>
        )}
      </Nav>
    </Navbar.Collapse>
  </Navbar>
)

export default Header;
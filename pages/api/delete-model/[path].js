const { deleteData } = require('../_src/utils/databaseActions')

const deleteModelFn = async (req, res) => {
  const reqQuery = req.query;
  console.log("LOG-DEBUG: deleteModelFn -> reqQuery", reqQuery)
  const { path, name } = reqQuery;
  await deleteData(path, {
    name,
  });

  res.status(200).send(`Deleted ${path} ${name ? "with query " + name : "completely"}`)
}

module.exports = deleteModelFn;

const { writeData } = require('./_src/utils/databaseActions')

const createAPIFn = async (req, res) => {
  const reqBody = req.body;
  const { name, type, content } = reqBody;
  console.log("LOG-DEBUG: createAPIFn -> reqBody", reqBody)
  await writeData(type, {
    name,
    content
  });

  res.status(200).send(`Created ${name}`)
};

module.exports = createAPIFn;

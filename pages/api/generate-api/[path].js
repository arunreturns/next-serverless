const { generateSchema } = require('../_src/utils/generateSchema');

const generateAPIFn = async (req, res) => {
  const reqQuery = req.query;
  const { path, retain } = reqQuery;
  console.log("LOG-DEBUG: generateAPIFn -> reqQuery", reqQuery)
  console.log("LOG-DEBUG: generateAPIFn -> path", path);
  console.log("LOG-DEBUG: generateAPIFn -> retain", retain)
  await generateSchema(path, retain);
  res.status(200).send("Generated")
}

module.exports = generateAPIFn;

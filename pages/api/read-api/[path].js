const { readData } = require('../_src/utils/databaseActions');

const readAPIFn = async (req, res) => {
  let queryPath = '';
  let queryPrms = {};
  let queryOpts = {};
  const reqQuery = req.query;
  console.log("LOG-DEBUG: readAPIFn -> reqQuery", reqQuery)
  for (const param in reqQuery) {
    if (param === 'path')
      queryPath = reqQuery[param];
    else if (param.startsWith("_"))
      queryOpts[param] = reqQuery[param];
    else
      queryPrms[param] = reqQuery[param];
  }

  const data = await readData(queryPath, queryPrms, queryOpts);

  res.status(200).json({ data });
}

module.exports = readAPIFn

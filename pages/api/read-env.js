const readEnvFn = async (req, res) => {
  const {
    query: { name }
  } = req;

  console.log("LOG-DEBUG: readEnvFn -> name", name)
  console.log("LOG-DEBUG: readEnvFn -> process.env[name]", process.env[name])
  res.status(200).send(`Env ${name} => ${process.env[name]}`);
}

module.exports = readEnvFn

const { readData, writeAPI, deleteData } = require("../utils/databaseActions");

const generateSchema = async (schemaName, retain) => {

  try {
    console.log("LOG-DEBUG: generateSchema -> schemaName", schemaName)
    const models = await readData("models", {
      name: schemaName
    });
    console.log("LOG-DEBUG: generateSchema -> models", models)

    const schema = {
      type: "object",
      properties: {},
      definitions: {},
    };

    const jsf = require("json-schema-faker");

    // Add faker to json-schema-faker
    jsf.extend("faker", () => {
      const faker = require("faker");
      return faker;
    });

    // Add chance to json-schema-faker
    jsf.extend("chance", () => {
      const Chance = require("chance");
      return new Chance();
    });

    const definitions = await readData("definitions");
    console.log("LOG-DEBUG: generateSchema -> definitions", definitions)

    definitions.map(definition => schema.definitions[definition.name] = JSON.parse(definition.content))
    console.log("LOG-DEBUG: generateSchema -> schema.definitions", schema.definitions)
    models.map(model => schema.properties[model.name] = JSON.parse(model.content))
    console.log("LOG-DEBUG: generateSchema -> schema.properties", schema.properties)

    schema.required = [schemaName];
    console.log("LOG-DEBUG: generateSchema -> schema", schema)

    const jsfGeneratedSchema = jsf.generate(schema);
    console.log("LOG-DEBUG: generateSchema -> jsfGeneratedSchema", jsfGeneratedSchema)

    if (!retain)
      await deleteData(schemaName);

    await writeAPI(schemaName, {
      content: jsfGeneratedSchema,
    });

  } catch (error) {
    console.log("LOG-DEBUG: generateSchema -> error", error)
  }
};

module.exports = {
  generateSchema
};

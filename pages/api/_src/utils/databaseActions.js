const url = require("url");
const MongoClient = require("mongodb").MongoClient;

const connectToDatabase = async () => {
  const uri = process.env.MONGODB_URI;
  // Create the client
  const client = await MongoClient.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });
  // Get the db from Mongo
  const db = await client.db(url.parse(uri).pathname.substr(1));
  // Return the database
  return db;
}

const readData = async (collectionName, query = {}, options = {}) => {
  const db = await connectToDatabase();

  const collection = await db.collection(collectionName);
  console.log("LOG-DEBUG: readData -> path", collectionName)
  console.log("LOG-DEBUG: readData -> query", query)
  console.log("LOG-DEBUG: readData -> options", options)

  const queryOptions = options ? {
    "limit": parseInt(options._limit),
    "skip": parseInt(options._page),
    "sort": options._sort
  } : {};

  const data = await collection.find(query, queryOptions).toArray();

  return data;
}

const writeData = async (collectionName, body) => {
  console.log("LOG-DEBUG: writeData -> collectionName", collectionName)
  // console.log("LOG-DEBUG: writeData -> body", body)

  const db = await connectToDatabase();

  const collection = await db.collection(collectionName);

  const { name, content } = body;

  console.log("LOG-DEBUG: writeData -> name", name)
  await collection.updateOne({
    name
  }, {
    $set: {
      name, content: JSON.stringify(content)
    }
  }, { upsert: true })
}

const writeAPI = async (collectionName, body) => {
  console.log("LOG-DEBUG: writeAPI -> collectionName", collectionName)
  // console.log("LOG-DEBUG: writeAPI -> body", body)

  const db = await connectToDatabase();

  const collection = await db.collection(collectionName);

  const { content } = body;

  await collection.insertMany(content[collectionName])
}

const updateData = async (collectionName, query = {}) => {
  console.log("LOG-DEBUG: updateData -> collectionName", collectionName)
  console.log("LOG-DEBUG: updateData -> query", query)
  console.log("LOG-DEBUG: updateData -> NOT IMPLEMENTED")
}

const deleteData = async (collectionName, query = {}) => {
  console.log("LOG-DEBUG: deleteData -> collectionName", collectionName)
  console.log("LOG-DEBUG: deleteData -> query", query)

  const db = await connectToDatabase();

  const collection = await db.collection(collectionName);

  await collection.remove(query);
}

module.exports = {
  readData,
  writeData,
  writeAPI,
  updateData,
  deleteData
};

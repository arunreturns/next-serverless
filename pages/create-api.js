import Layout from "../layout/Layout";
import { Row, Col, Form, Button } from "react-bootstrap";
import { Formik } from 'formik';
import * as yup from 'yup';

const schema = yup.object({
  name: yup.string().required(),
  type: yup.string().required(),
  content: yup.string().required(),
});

const BASE_URL = process.env.BASE_URL;
const ESCAPE_REGEX = /(\r\n|\n|\r|\t|\t\t)/gi;

const CreateAPI = ({ formGroupClass, formRowClass }) => {
  const createAPI = async (values) => {
    const requestValues = { ...values };
    const escapedString = requestValues.content.toString().replace(ESCAPE_REGEX, "")
    console.log("LOG-DEBUG: createAPI -> escapedString", escapedString)
    requestValues.content = escapedString;
    console.log("LOG-DEBUG: createAPI -> requestValues", requestValues)
    await fetch(`${BASE_URL}/create-api`, {
      method: 'post',
      body: JSON.stringify(requestValues),
      headers: new Headers({
        'content-type': 'application/json'
      })
    })
  }
  return <Layout title={"Create API"}>
    <Row>
      <Col md={{ span: 6, offset: 3 }}>
        <Formik
          validationSchema={schema}
          onSubmit={(values) => createAPI(values)}
          initialValues={{
            name: '',
            type: '',
            content: '',
          }}
        >
          {({
            handleSubmit,
            handleChange,
            handleBlur,
            values,
            touched,
            isValid,
            errors,
          }) => (
              <Form noValidate onSubmit={handleSubmit}>
                <Form.Group controlId="name" className={formGroupClass}>
                  <Form.Label>File Name</Form.Label>
                  <Form.Control type="text"
                    placeholder="Enter file name"
                    value={values.name}
                    onChange={handleChange}
                    isValid={touched.name && !errors.name}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.name}
                  </Form.Control.Feedback>
                </Form.Group>

                <Form.Group controlId="type" className={formGroupClass}>
                  <Form.Label>File Type</Form.Label>
                  <Form.Control type="text" placeholder="Enter file type"
                    value={values.type}
                    onChange={handleChange}
                    isValid={touched.type && !errors.type}
                  />
                </Form.Group>

                <Form.Group controlId="content" className={formGroupClass}>
                  <Form.Label>File Content</Form.Label>
                  <Form.Control as="textarea" rows="10" placeholder="Enter file content"
                    value={values.content}
                    onChange={handleChange}
                    isValid={touched.content && !errors.content} />
                </Form.Group>

                <Form.Group className={formGroupClass}>
                  <Button variant="primary" type="submit">
                    Submit
                  </Button>
                </Form.Group>
              </Form>
            )}
        </Formik>
      </Col>
    </Row>
  </Layout>
}

CreateAPI.defaultProps = {
  formGroupClass: 'd-flex flex-column flex-fill text-left',
}

export default CreateAPI;
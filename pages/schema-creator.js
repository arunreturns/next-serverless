import { useState } from 'react';
import { Formik } from 'formik';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import * as yup from 'yup';

import Layout from "../layout/Layout";
import { Form, Button, ButtonGroup, InputGroup, Row, Col } from "react-bootstrap";
import { createNestedObject, getDeepKeys, setNestedValue, removeNestedValue, getNestedObject, getTypeofValue } from '../utils';

const schema = yup.object({
  key: yup.string(),
  value: yup.string(),
});

const SchemaCreator = ({ formGroupClass }) => {
  const [schemaObj, setSchemaObj] = useState()
  const [objectKeys, setObjectKeys] = useState(getDeepKeys(schemaObj))

  const types = [
    { label: 'Object', value: 'Object' },
    { label: 'String', value: 'String' },
    { label: 'Number', value: 'Number' },
    { label: 'Boolean', value: 'Boolean' },
    { label: 'Array', value: 'Array' },
    { label: 'Array-Push', value: 'Array-Push' },
  ];

  const BASE_URL = process.env.BASE_URL;

  const getDetails = async (values, resetForm) => {
    const URL = `${BASE_URL}/read-api/${values.type}/?name=${values.name}`;
    console.log("LOG-DEBUG: getDetails -> values", values)
    console.log("LOG-DEBUG: getDetails -> URL", URL)
    const response = await fetch(URL);
    const content = await response.json();
    console.log("LOG-DEBUG: getDetails -> content", content)
    resetForm();
    const data = content.data.pop();
    console.log("LOG-DEBUG: getDetails -> data.content", data.content)
    const contentObj = JSON.parse(data.content);
    setSchemaObj(contentObj);
    setObjectKeys(getDeepKeys(contentObj));
  }

  const createAPI = async (values, actions) => {
    console.log("LOG-DEBUG: createAPI -> values", values)
    await fetch(`${BASE_URL}/create-api`, {
      method: 'POST',
      body: JSON.stringify(values),
      headers: new Headers({
        'content-type': 'application/json'
      })
    })
    actions.resetForm();
  }

  const updateObject = ({ parentKey, key, value, type }, { resetForm }) => {
    console.log("LOG-DEBUG: updateObject -> parentKey", parentKey)
    console.log("LOG-DEBUG: updateObject -> key", key)
    console.log("LOG-DEBUG: updateObject -> value", value)
    console.log("LOG-DEBUG: updateObject -> type", type)
    try {
      const deepObj = { ...schemaObj };
      const nestedKeys = parentKey ? parentKey.split(".") : [];
      const nestedVal = createNestedObject(deepObj, nestedKeys);
      const valueType = getTypeofValue(nestedVal);
      if (valueType)
        setNestedValue(deepObj, nestedKeys, nestedKeys.pop(), key, valueType);
      else
        setNestedValue(deepObj, nestedKeys, key, value, type);
      const updatedObj = { ...schemaObj, ...deepObj };
      setSchemaObj(updatedObj)
      setObjectKeys(getDeepKeys(updatedObj));
      resetForm();
    } catch (error) {
      console.log("LOG-DEBUG: updateObject -> error", error)
    }
  }

  const removeKey = ({ parentKey }, resetForm) => {
    try {
      const deepObj = { ...schemaObj };
      removeNestedValue(deepObj, parentKey.split("."))
      const updatedObj = { ...deepObj };
      setSchemaObj(updatedObj)
      setObjectKeys(getDeepKeys(updatedObj));
      resetForm();
    } catch (error) {
      console.log("LOG-DEBUG: removeKey -> error", error)
    }
  }

  const copyKey = ({ parentKey, key }, resetForm) => {
    try {
      const deepObj = { ...schemaObj };
      const nestedKeys = parentKey ? parentKey.split(".") : [];
      const value = getNestedObject(deepObj, nestedKeys);
      nestedKeys.pop();
      setNestedValue(deepObj, nestedKeys, key, value);
      const updatedObj = { ...schemaObj, ...deepObj };
      setSchemaObj(updatedObj)
      setObjectKeys(getDeepKeys(updatedObj));
      resetForm();
    } catch (error) {
      console.log("LOG-DEBUG: copyKey -> error", error)
    }
  }

  return (
    <Layout title={"Schema Creator"}>
      <Row>
        <Col xs={12} sm={6}>
          <Formik
            validationSchema={schema}
            onSubmit={(values, actions) => updateObject(values, actions)}
            enableReinitialize={true}
            initialValues={{
              parentKey: '',
              key: '',
              type: 'Object',
              value: '',
            }}
          >{({
            handleSubmit,
            handleChange,
            setFieldValue,
            values,
            touched,
            errors,
            resetForm
          }) => (
              <Form onSubmit={handleSubmit}>
                {
                  objectKeys.length > 0 && <Form.Group controlId="parentKey" className={`${formGroupClass} flex-column`}>
                    <Form.Label>Parent Key</Form.Label>
                    <InputGroup>
                      <Form.Control as={"select"}
                        placeholder="Enter Parent Key"
                        value={values.parentKey}
                        onChange={handleChange}
                        isValid={touched.parentKey && !errors.parentKey}
                      >
                        <option>{""}</option>
                        {objectKeys.map(objectKey => <option key={objectKey}>{objectKey}</option>)}
                      </Form.Control>
                      {values.parentKey && <InputGroup.Append>
                        <Button variant="danger" onClick={() => removeKey(values, resetForm)}>
                          <i className="fas fa-trash"></i>
                        </Button>
                      </InputGroup.Append>}
                    </InputGroup>
                    <Form.Control.Feedback type="invalid">
                      {errors.parentKey}
                    </Form.Control.Feedback>
                  </Form.Group>
                }

                {
                  values.type !== 'Array-Push' && <Form.Group controlId="key" className={`${formGroupClass} flex-column`}>
                    <Form.Label>Key</Form.Label>
                    <Form.Control type="text"
                      placeholder="Enter key"
                      value={values.key}
                      onChange={handleChange}
                      isValid={touched.key && !errors.key}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.key}
                    </Form.Control.Feedback>
                  </Form.Group>
                }

                {
                  (values.type !== 'Object') &&
                  <Form.Group controlId="value" className={`${formGroupClass} flex-column`}>
                    <Form.Label>Value</Form.Label>
                    <Form.Control type="text"
                      placeholder="Enter value"
                      value={values.value}
                      onChange={handleChange}
                      isValid={touched.value && !errors.value}
                    />
                    <Form.Control.Feedback type="invalid">
                      {errors.value}
                    </Form.Control.Feedback>
                  </Form.Group>
                }

                <Form.Group controlId="type" className={`${formGroupClass} flex-column`}>
                  <Form.Label>Type</Form.Label>
                  <ButtonGroup type={"radio"}>{types.map((type, idx) =>
                    <Button key={type.value + idx} variant={`${type.value === values.type ? "dark" : "light"}`}
                      onClick={() => setFieldValue("type", type.value)}>
                      {type.label}
                    </Button>
                  )}</ButtonGroup>
                </Form.Group>

                <Form.Group controlId="type" className={`${formGroupClass} flex-row`}>
                  <Button className="flex-grow-1" variant="primary" type="submit">Add Key</Button>
                  <Button className="flex-grow-1" variant="success" onClick={() => copyKey(values, resetForm)}>Copy Key</Button>
                  <Button className="flex-grow-1" variant="danger" onClick={() => resetForm()}>Reset</Button>
                </Form.Group>
              </Form>
            )}
          </Formik>
        </Col>
        <Col xs={12} sm={6}>
          <Formik
            validationSchema={schema}
            onSubmit={(values, actions) => createAPI(values, actions)}
            enableReinitialize={true}
            initialValues={{
              name: '',
              type: '',
              content: schemaObj,
            }}
          >{({
            handleSubmit,
            handleChange,
            values,
            touched,
            errors,
            resetForm,
          }) => (
              <Form onSubmit={handleSubmit}>
                <Form.Group controlId="name" className={`${formGroupClass} flex-column`}>
                  <Form.Label>File Name</Form.Label>
                  <Form.Control type="text"
                    placeholder="Enter file name"
                    value={values.name}
                    onChange={handleChange}
                    isValid={touched.name && !errors.name}
                  />
                </Form.Group>

                <Form.Group controlId="type" className={`${formGroupClass} flex-column`}>
                  <Form.Label>File Type</Form.Label>
                  <Form.Control type="text" placeholder="Enter file type"
                    value={values.type}
                    onChange={handleChange}
                    isValid={touched.type && !errors.type}
                  />
                </Form.Group>

                <Form.Group controlId="type" className={`${formGroupClass} flex-row`}>
                  <Button className="flex-grow-1" variant="primary" type="submit">Create Schema</Button>
                  <span className="mx-2"></span>
                  <Button className="flex-grow-1" variant="success" type="reset" onClick={() => getDetails(values, resetForm)}>Get Details</Button>
                </Form.Group>
                <div className="float-right">
                  <CopyToClipboard text={JSON.stringify(schemaObj, null, 2)}>
                    <Button variant={"info"}><i className="far fa-copy"></i></Button>
                  </CopyToClipboard>
                </div>
                <div className="float-right">
                  <Button variant={"danger"}><i className="fa fa-window-close" onClick={() => setSchemaObj()}></i></Button>
                </div>
              </Form>
            )}
          </Formik>
          <pre className="font-source-code-pro">{JSON.stringify(schemaObj, null, 2)}</pre>
        </Col>
      </Row>
    </Layout >

  )
}

SchemaCreator.defaultProps = {
  // formRowClass: 'd-flex justify-content-center',
  formGroupClass: 'd-flex flex-fill text-left',
}

export default SchemaCreator;
import React from 'react';
import App from 'next/app';
import Head from 'next/head';
import 'bootstrap/dist/css/bootstrap.css';

class CustomApp extends App {
  render() {
    const { Component, pageProps } = this.props
    return <React.Fragment>
      <Head>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link
          href="https://fonts.googleapis.com/css?family=Cabin"
          rel="stylesheet"
          key="google-font-cabin"
        />
        <link
          href="https://fonts.googleapis.com/css?family=Fascinate+Inline&display=swap"
          rel="stylesheet"
          key="google-font-fascinate-inline"
        />
        <link
          href="https://fonts.googleapis.com/css?family=Source+Code+Pro&display=swap"
          rel="stylesheet"
          key="google-font-source-code-pro"
        />
        <link
          href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          rel="stylesheet"
          key="font-awesome-icons"
        />
      </Head>
      <Component {...pageProps} />
      <style global jsx>{`
        body {
          font-family: 'Cabin', sans-serif;
        }
        .font-cabin {
          font-family: 'Cabin', sans-serif;
        }
        .fascinate-inline {
          font-family: 'Fascinate Inline', sans-serif;
        }
        .font-source-code-pro {
          font-family: 'Source Code Pro', sans-serif;
        }
      `}</style>
    </React.Fragment>
  }
}

export default CustomApp;
export const getDeepKeys = (obj) => {
  let keys = [];
  for (let key in obj) {
    keys.push(key);
    if (typeof obj[key] === "object") {
      let subKeys = getDeepKeys(obj[key]);
      keys = keys.concat(subKeys.map(function (subKey) {
        return key + "." + subKey;
      }));
    }
  }
  return keys;
}

export const createNestedObject = (base, keys) => {
  if (keys.length === 0) return;
  for (let i = 0; i < keys.length; i++) {
    base = base[keys[i]] = base[keys[i]] || {};
  }
  return base;
};

export const getNestedObject = (base, keys) => {
  const nestedObj = keys.reduce((o, x) => o && o[x], base);
  return JSON.parse(JSON.stringify(nestedObj));
};

export const setNestedValue = (base, keys, key, value, type) => {
  for (let i = 0; i < keys.length; i++) {
    base[keys[i]] && (base = base[keys[i]])
  }
  if (type === "Array-Push")
    base.push(value);
  else if (type === "Array")
    base[key] = value.split(",")
  else if (type === "String")
    base[key] = value
  else if (type === "Number")
    base[key] = Number(value)
  else if (type === "Boolean")
    base[key] = Boolean(value)
  else if (type === "Object")
    base[key] = {};
  else
    base[key] = value
}

export const removeNestedValue = (base, keys) => {
  const lastKey = keys.pop();
  for (let i = 0; i < keys.length; i++) {
    base[keys[i]] && (base = base[keys[i]])
  }
  if (Array.isArray(base))
    base.splice(lastKey, 1)
  else
    delete base[lastKey];
}

export const getTypeofValue = (value) => {
  try {
    if (!isNaN(parseFloat(value)))
      return "Number"
  } catch (e) { }
  try {
    if (!isNaN(parseInt(value)))
      return "Number"
  } catch (e) { }

  if (value === "true" || value === "false") return "Boolean"

  if (typeof value === 'string' || value instanceof String) return "String";
}
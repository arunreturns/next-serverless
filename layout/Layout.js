import Header from "../components/Header";
import Head from 'next/head';
import { Container } from "react-bootstrap";

const Layout = props => (
  <React.Fragment>
    <Head>
      <title>{props.title}</title>
      <meta name="description" content={props.title} />
    </Head>
    <Header />
    <Container fluid className="mt-3">
      {props.children}
    </Container>
  </React.Fragment>
)

export default Layout;